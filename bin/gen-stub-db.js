#!/usr/bin/env node

const
  config = require('../config'),
  _ = require('lodash'),
  q = require('q'),
  ora = require('ora'),
  knex = require('knex'),
  { expect } = require('chai');



const spinner = ora('Starting ...').start();
var env = {};

q()
.then(() => expect(_.get(config, 'db.client')).equal('sqlite3', 'Please use config-stub.js config file'))
.then(() => {
  spinner.start('Running ...');
  env.db = db = knex(config.db);
})
.then(() => env.db.schema.dropTableIfExists('SAMPLES'))
.then(() => env.db.schema.createTable('SAMPLES', function(table) {
  table.increments('SMP_ID');
  table.text('SMP_TITLE');
  table.text("SMP_DESCRIPTION");
  table.text("SMP_ELEMENT");
  table.integer("SMP_MASSNUMBER");
  table.text("SMP_SETUP");
  table.integer("SMP_OLDID");
  table.text("SMP_EXPERIMENTAREA");
  table.integer("SMP_MASSMG");
  table.integer("SMP_DIAMETERMM");
}))
.then(() => spinner.succeed('SAMPLES created'))
.then(() => env.db('SAMPLES').insert([
  { SMP_ID: 1, SMP_TITLE: 'test', SMP_DESCRIPTION: 'blah', SMP_ELEMENT: 'Ge', SMP_MASSNUMBER: 42 }
]))
.then(() => spinner.succeed('SAMPLES populated'))

.then(() => env.db.schema.dropTableIfExists('FILTERS'))
.then(() => env.db.schema.createTable('FILTERS', function(table) {
  table.increments('FLT_ID');
  table.text('FLT_TITLE');
  table.text("FLT_DESCRIPTION");
  table.text("FLT_ELEMENT");
  table.text('FLT_POSITION');
  table.integer("FLT_MASSNUMBER");
  table.text("FLT_ACTIVE");
  table.text("FLT_EXPERIMENTAREA");
  table.integer("FLT_MASSMG");
  table.integer("FLT_DIAMETERMM");
}))
.then(() => spinner.succeed('FILTERS created'))
.then(() => env.db('FILTERS').insert([
  { FLT_ID: 1, FLT_TITLE: 'test', FLT_DESCRIPTION: 'blah', FLT_ELEMENT: 'Ge', FLT_MASSNUMBER: 42 }
]))
.then(() => spinner.succeed('FILTERS populated'))


.then(() => env.db.schema.dropTableIfExists('SOURCES'))
.then(() => env.db.schema.createTable('SOURCES', function(table) {
  table.increments('SRC_ID');
  table.text('SRC_TITLE');
  table.text("SRC_DESCRIPTION");
  table.text("SRC_ELEMENT");
  table.integer("SRC_MASSNUMBER");
  table.text("SRC_SETUP");
  table.text("SRC_EXPERIMENTAREA");
  table.integer("SRC_MASSMG");
  table.integer("SRC_DIAMETERMM");
}))
.then(() => spinner.succeed('SOURCES created'))
.then(() => env.db('SOURCES').insert([
  { SRC_ID: 12345, SRC_TITLE: 'test', SRC_DESCRIPTION: 'blah', SRC_ELEMENT: 'Ge', SRC_MASSNUMBER: 42 }
]))
.then(() => spinner.succeed('SOURCES populated'))

.then(() => env.db.schema.dropTableIfExists('RUNS'))
.then(() => env.db.schema.createTable('RUNS', function(table) {
  table.increments('RUN_ID');
  table.text('RUN_EXPERIMENTAREA');
  table.date('RUN_START');
  table.date('RUN_STOP');
  table.text('RUN_TITLE');
  table.text('RUN_DESCRIPTION');
  table.integer('RUN_NBSEG');
  table.integer('RUN_NBCHANNELS');
  table.integer('STP_ID');
  table.integer('RUN_TOTPROT3');
  table.integer('RUN_FILEEXIST');
  table.integer("RUN_OLDID");
  table.integer('RUND_IDNTOF');
  table.integer('RUN_TOTPROT');
  table.text('RUN_CASTORPATH');
  table.integer('RUN_FLAG');
  table.integer('RUN_FLAGSTATUS');
}))
.then(() => spinner.succeed('RUNS created'))
.then(() => env.db('RUNS').insert([
  { RUN_ID: 16219,
    RUN_EXPERIMENTAREA: 'EAR1',
    RUN_START: new Date('2012-11-09T21:32:00'),
    RUN_STOP: new Date('2012-11-09T21:48:00'),
    RUN_TITLE: 'K6D6: Pu240 sample Beam ON [threshold lowered two channels] (Bicron C6D6 and BaF2 plugged)',
    RUN_DESCRIPTION: 'K6D6: Pu240 sample Beam ON [threshold lowered two channels] (Bicron C6D6 and BaF2 plugged)<br/>240Pu(n,g) with C6D6 bricron (DAQ setup)<br/>Detectors not going through Level Shifter!<br/>--------------------<br/>Sample in Beam: 1<br/>Filters in Beam: none<br/>--------------------<br/>Samples in sample exchanger:<br/>143 Carbon 22 mm diam, 10 mm thick <br/>1   Nothing (not even a frame)<br/>139 Strontium-87, 1 inch, 0.287 g<br/>140 Silver, 1 inch, ??g<br/>End of file<br/><br/>--------------------<br/>High Voltages:<br/>- old crate not used -<br/>End of file',
    RUN_NBSEG: 1, RUN_NBCHANNELS: 16,
    RUN_TOTPROT3: 606283000000000, RUN_FILEEXIST: 1,
    RUN_OLDID: 15669, RUND_IDNTOF: 15669, RUN_TOTPROT: 606283000000000,
    RUN_CASTORPATH: 'No information', RUN_FLAG: -2 },
    { RUN_ID: 38912,
      RUN_EXPERIMENTAREA: 'EAR1',
      RUN_START: new Date('2018-11-22T16:44:56'), RUN_STOP: new Date('2018-11-22T17:48:59'),
      RUN_TITLE: 'SPDevies test', RUN_DESCRIPTION: 'SPDevices with standard driver',
      RUND_IDNTOF: 110931, RUN_CASTORPATH: 'daq_test2018',
      RUN_FLAG: -1, RUN_FLAGSTATUS: 0 }
]))
.then(() => spinner.succeed('RUNS populated'))

.then(() => env.db.schema.dropTableIfExists('ELOGBOOK'))
.then(() => env.db.schema.createTable('ELOGBOOK', function(table) {
  table.increments('ELOG_ID');
  table.date('ELOG_DATE');
  table.text('ELOG_COMMENTS');
  table.integer('RUN_ID');
  table.integer('USER_ID');
  table.integer('ELOG_OLDID');
}))
.then(() => spinner.succeed('ELOGBOOK created'))
.then(() => env.db('ELOGBOOK').insert([
  { "ELOG_ID":3779,
    "ELOG_DATE": "2018-11-09T16:52:02.812Z",
    "ELOG_COMMENTS":"HIPS stop set to 570 and 610 at trigger 1500",
    "RUN_ID":16219,"USER_ID":227,"ELOG_OLDID":null },
  { "ELOG_ID":3780,
    "ELOG_DATE":"2018-11-09T18:37:55.361Z",
    "ELOG_COMMENTS":"HIPS stop set to 570 and 610 at trigger 1500. Tripping at trigger 2780. Turned off. Brought back up after 10 minutes.",
    "RUN_ID":16219,"USER_ID":228,"ELOG_OLDID":null }
]))
.then(() => spinner.succeed('ELOGBOOK populated'))

.then(
  () => spinner.succeed('done'),
  (err) => spinner.fail(err)
)
.finally(() => env.db.destroy())
