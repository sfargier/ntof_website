/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-02-01T08:48:10+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

var _ = require('lodash');

/**
 * push an element in an array, mutating the array or creating a new one if it's
 * not an array (null or undefined).
 * @param  {array} list     the array to push in
 * @param  {any}   element  the element to insert
 * @return {array}          the mutated or created array
 */
_.push = function(list, element) {
  if (!_.isArray(list)) {
    return [ element ];
  }
  list.push(element);
  return list;
};

/**
 * bind method not accepting any additional parameter
 * @example
 * _.bind0(func, thisArg, ...args)
 * // is equivalent to:
 * _.ary(_.bind(func, thisArg, ...args), 0)
 */
_.bind0 = function(func, thisArg, ...args) {
  return _.ary(_.bind(func, thisArg, ...args), 0);
};

/**
 * bind method using its name
 * @param  {object} object     object to bind method from
 * @param  {string} methodName method to bind
 * @param  {} args             arguments to bind
 * @return {function}          bound function
 */
_.mbind = function(object, methodName, ...args) {
  return _.bindKey(object, methodName, ...args);
};

_.fbind = _.partial;
/**
 * bind function not accepting any additional parameter
 * @param  {function} func function to bind
 * @param  {} args         arguments to bind
 * @return {function}      bound function
 */
_.fbind0 = function(func, ...args) {
  return _.ary(_.partial(func, ...args), 0);
};

/**
 * bind a method using its name
 * @param  {object} object     object to bind method from
 * @param  {string} methodName method to bind
 * @param  {} args             arguments to bind
 * @return {function}          bound function
 */
_.mbind0 = function(object, methodName, ...args) {
  return _.ary(_.mbind(object, methodName, ...args), 0);
};

module.exports = _;
