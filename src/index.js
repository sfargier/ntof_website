
const
  debug = require('debug')('app:server'),
  express = require('express'),
  path = require('path'),
  reload = require('reload'),
  swagger = require('./swagger'),
  config = require('../config'),
  db = require('./db');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use('/public', express.static(path.join(__dirname, '..', 'public')));

app.get('/', (req, res) => res.render('index'));

app.use('/db', db.router(config));

swagger.register(app);

reload(app, { verbose: true });

/**
 * global swagger doc
 * @swagger
 * tags: [
 *   { name: 'db', description: 'database general API' }
 * ]
 */


if (!module.parent) {
  /* we're called as a main, let's listen */
  debug("Starting server");
  app.listen(8080, () => console.log("Server is now listening !"));
} else {
  /* export our server */
  module.exports = app;
}
