
const
  debug = require('debug')('rest:helpers'),
  _ = require('lodash');

class HTTPError extends Error {
  constructor(status, message) {
    super(message);
    this.status = status;

    switch (Math.floor(status / 100)) {
      case 4:
        this.code = 'HTTPClientError';
        break;
      case 5:
        this.code = 'HTTPServerError';
        break;
      default:
        this.code = 'HTTPError';
        break;
    }
  }
}

function jsonRet(res, ret) {
  return res.json(ret);
}

function errorRet(res, ret) {
  debug('error:', JSON.stringify(ret));
  res
  .status(_.get(ret, 'status', 500))
  .send(_.get(ret, 'message', ret));
}

function promiseWrapper(fun) {
  return function(req, res, next) {
    var ret = fun(req, res, next);
    if (_.get(ret, 'then', false)) {
      return ret.then(
      jsonRet.bind(null, res),
      errorRet.bind(null, res));
    }
    return ret;
  };
}

module.exports = {
  HTTPError,
  wrap: promiseWrapper
};
