
const
  _ = require('./lodash-ext');

class KnexRESTSwagger {
  constructor(swaggerDoc, extraDoc) {
    this.doc = swaggerDoc || {};
    this.extra = extraDoc;
  }

  swagTemplate(service, template, path) {

    var doc = _.cloneDeep(_.get(this.doc, [ 'paths', template ]));
    _.forEach(doc, (elt) => {
      elt.tags = [ service.path ];
      /* remove resolved parameters */
      _.remove(elt.parameters, { $ref: '#/parameters/dbTable' });
      _.remove(elt.parameters, { $ref: '#/parameters/dbRelation' });
    });

    _.forEach(_.get(this.extra, [ 'paths', path ]), (value, key) => {
      _.set(doc, key, value);
    });
    _.set(this.doc, [ 'paths', path ], doc);
    return doc;
  }

  /**
   * @brief generate swagger doc for given service
   */
  swag(service) {
    var basePath = _.get(this.extra, 'basePath', '');
    var path = basePath + '/' + _.get(service, 'path');
    var doc;

    var tag = _.get(this.extra, [ 'tags', path ]);
    if (!_.isNil(tag)) {
      this.doc.tags = _.push(this.doc.tags,
        { name: service.path, description: tag });
    }

    /* table */
    this.swagTemplate(service, '/db/{table}', path);
    this.swagTemplate(service, '/db/{table}/count', path + '/count');

    /* items */
    if (service.options.key) {
      this.swagTemplate(service, '/db/{table}/item/{id}', path + '/item/{id}');

      if (service.options.related) {
        doc = this.swagTemplate(service, '/db/{table}/item/{id}/related',
          path + '/item/{id}/related');
        _.set(doc, "get.responses.200.examples.[application/json]",
          _.keys(service.options.related));
      }
      _.forEach(service.options.related, (desc, name) => {
        this.swagTemplate(service, '/db/{table}/item/{id}/related/{relation}',
          path + '/item/{id}/related/' + name);
        this.swagTemplate(service, '/db/{table}/item/{id}/related/{relation}/count',
          path + '/item/{id}/related/' + name + '/count');
      });
      return this.doc;
    }
  }
}

module.exports = KnexRESTSwagger;
