const
  _ = require('lodash'),
  path = require('path'),
  swaggerJSDoc = require('swagger-jsdoc'),
  swaggerUi = require('swagger-ui-express');

const options = {
  definition: {
    info: {
      title: 'n_TOF service',
      version: '1.0.0'
    }
  },
  // Path to the API docs
  apis: [ path.join(__dirname, '**.js') ]
};

// Initialize swagger-jsdoc -> returns validated swagger spec in json format
var swaggerSpec;

const Swagger = module.exports = {
  getSpec() {
    if (!swaggerSpec) {
      swaggerSpec = swaggerJSDoc(options);
    }
    return swaggerSpec;
  },

  register(app) {
    app.get('/api-docs.json', (req, res) => {
      res.json(swaggerSpec);
    });

    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(
      Swagger.getSpec(),
      { swaggerOptions: { docExpansion: "none", displayRequestDuration: true } }
    ));
  }
};
