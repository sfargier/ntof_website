
const
  debug = require('debug')('rest:db'),
  _ = require('./lodash-ext'),
  q = require('q'),
  { HTTPError, wrap } = require('./ExpressHelpers');

/**
 * @swagger
 * parameters:
 *   dbTable:
 *     name: table
 *     in: path
 *     required: true
 *     description: requested table
 *     schema: {}
 *   dbItem:
 *     name: id
 *     description: the item id
 *     in: path
 *     required: true
 *     schema: {}
 *   dbRelation:
 *     name: relation
 *     description: related items table name
 *     in: path
 *     required: true
 *     schema: {}
 */

const parseOperators = {
  eq: '=', ne: '<>',
  gt: '>', gte: '>=',
  lt: '<', lte: '<=',
  in: 'in'
};

/**
 * KnexREST SQL REST wrapper
 */
class KnexREST {
  /**
   * KnexREST constructor
   * @param {Object} knex    Knex DB connection object
   * @param {String} table   Table to wrap (also used as default path)
   * @param {Object} options Wrapper options (see details)
   *
   * @details Available options:
   * - key: primary key column name
   * - path: the HTTP path for this database
   * - table: override the table argument (using it only for path)
   * - mapping: column public name mapping (column: public_name)
   * - strict: check that all filter/sort criterias match a mapping
   * - related: related tables description
   */
  constructor(knex, table, options) {
    this.knex = knex;
    this.options = _.pick(options, [ 'mapping', 'path', 'table', 'key',
      'strict', 'related' ]);
    this.table = _.get(this.options, 'table', table);
    this.path = _.get(this.options, 'path', table);

    if (_.has(this.options, 'mapping')) {
      _.set(this.options, 'revmapping', _.invert(this.options.mapping));

      /* default key mapping */
      if (this.options.key && !_.has(this.options.mapping, this.options.key)) {
        _.set(this.options.revmapping, this.options.key, this.options.key);
        _.set(this.options.mapping, this.options.key, this.options.key);
      }
    }

    if (!_.includes(KnexREST.tables, this.path)) {
      KnexREST.tables = _.push(KnexREST.tables, this.path);
    }
    debug('DB wrapper for:', this.path);
  }

  /**
   * @swagger
   * /db/{table}:
   *   get:
   *     tags: ['db']
   *     summary: retrieve or search for items
   *     description: 'retrieve or search for items in a database<br/><br/>
   *       **Note:** both *sort* and *filter* can be repeated in query string or multi-valued using csv (comma) separator:<br/>
   *         - filter=*columnName*:*value*&filter=*columnName2*:*value2*<br/>
   *         - sort=*columnName*,*columnName2*:asc<br/>'
   *     produces: application/json
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - name: max
   *         description: max items in result
   *         in: query
   *         required: false
   *         type: integer
   *         default: 100
   *       - name: offset
   *         description: result's offset
   *         in: query
   *         required: false
   *         type: integer
   *         default: 0
   *       - name: filter
   *         collectionFormat: multi
   *         description: 'filtering rules syntax:<br/>
   *           - filter=*columnName*:*value*<br/>
   *           - filter[*columnName*]=*value*<br/>
   *           - filter[*columnName*][gte]=*value*<br/><br/>
   *           supported operators:<br/>
   *           - ne: not equal<br/>
   *           - gt, gte: greater (or equal) than<br/>
   *           - lt, lte: less than<br/>'
   *         in: query
   *         required: false
   *       - name: sort
   *         description: 'results order:<br/>
   *           - sort=*columnName* (defaults to asc)<br/>
   *           - sort=*columnName*:asc<br/>
   *           - sort=*columnName*:desc<br/>'
   *         in: query
   *         required: false
   *     responses:
   *       200:
   *         description: items from {table}
   *         examples:
   *           application/json:
   *             - id: 42
   *               name: sample
   *               color: blue
   *             - id: 44
   *               name: test
   *               color: yellow
   */
  search(req, res) { /* jshint unused:false */
    return q() /* wrap in q to handle exceptions */
    .then(() => {
      var query = this.knex(this.table).select();
      query = this._parseFilter(query, _.get(req, 'query.filter'));
      query = this._parseSort(query, _.get(req, 'query.sort'));

      if (this.options.strict && this.options.revmapping) {
        query = query.column(this.options.revmapping);
      }

      return query
      .limit(_.get(req, 'query.max', 100))
      .offset(_.get(req, 'query.offset', 0))
      .then(
        (ret) => {
          if (!this.options.strict && this.options.mapping) {
            return _.map(ret, (item) =>
              _.mapKeys(item, (value, key) => this._fromDB(key)));
          }
          return ret;
        }
      );
    });
  }

  /**
   * @swagger
   * /db/{table}/item/{id}:
   *   get:
   *     tags: ['db']
   *     summary: retrieve a specific item
   *     description: retrieve a specific item
   *     produces: application/json
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *     responses:
   *       200:
   *         description: the requested item
   *         examples:
   *           application/json:
   *             id: 42
   *             name: sample
   *             color: blue
   *       404:
   *         description: item does not exist
   */
  get(req, res) { /* jshint unused:false */
    var query = this.knex(this.table).first();
    if (this.options.strict && this.options.revmapping) {
      query = query.column(this.options.revmapping);
    }

    return query
    .where(this.options.key, _.get(req.params, 'id'))
    .then(
      (ret) => {
        if (_.isNil(ret)) {
          throw new HTTPError(404);
        }
        if (!this.options.strict && this.options.mapping) {
          return _.map(ret, (item) =>
            _.mapKeys(item, (value, key) => this._fromDB(key)));
        }
        return ret;
      }
    );
  }

  /**
   * @swagger
   * /db/{table}/item/{id}/related/{relation}:
   *   get:
   *     tags: ['db']
   *     summary: retrieve related items
   *     description: '
   *     Shorthand method to query another table for related items.<br/><br/>
   *       **Note:** the same query parameters than on a direct table query can be used'
   *     produces: application/json
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *       - $ref: '#/parameters/dbRelation'
   *     responses:
   *       200:
   *         description: related items
   *         examples:
   *           application/json:
   *             - id: 42
   *               name: sample
   *               color: blue
   *             - id: 44
   *               name: test
   *               color: yellow
   *
   * /db/{table}/item/{id}/related/{relation}/count:
   *   get:
   *     tags: ['db']
   *     summary: get related items count
   *     description: 'get number of related items.'
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *       - $ref: '#/parameters/dbRelation'
   *     responses:
   *       200:
   *         description: item count
   *         examples:
   *           application/json: 42
   */
  related(path, key, router, req, res) {
    req.url = path;
    _.set(req, [ 'query', 'filter', key ], _.get(req.params, 'id'));
    router.handle(req, res);
  }

  relatedJoin(path, key, joinInfo, router, req, res) {
    var sel = this.knex(joinInfo.table)
    .select(joinInfo.relKey)
    .where(joinInfo.key, _.get(req.params, 'id'));
    _.set(req, [ 'query', 'filter', key, 'in' ], sel);
    req.url = path;
    router.handle(req, res);
  }

  /**
   * @swagger
   * /db/{table}/item/{id}/related:
   *   get:
   *     tags: ['db']
   *     summary: list available relations
   *     description: list available relations
   *     produces:
   *       - application/json
   *     parameters:
   *       - $ref: '#/parameters/dbTable'
   *       - $ref: '#/parameters/dbItem'
   *     responses:
   *       200:
   *         description: list available relations
   *         schema:
   *           type: array
   *           items:
   *             type: string
   *         examples:
   *           application/json: [ 'test', 'sample' ]
   */
  listRelated(req, res) { /* jshint unused:false */
    return q(_.keys(this.options.related));
  }

  /**
   * @swagger
   * /db/{table}/count:
   *   get:
   *     tags: ['db']
   *     summary: get number of items in the table
   *     description: 'get number of items matching given filter rules or total
   *      number of items in the table.'
   *     produces:
   *       - application/json
   *     parameters:
   *       - table:
   *         $ref: '#/parameters/dbTable'
   *       - name: filter
   *         description: filtering rule, see /db/{table} for details
   *         in: query
   *         required: false
   *         collectionFormat: multi
   *     responses:
   *       200:
   *         description: item count
   *         examples:
   *           application/json: 42
   */
  count(req, res) { /* jshint unused:false */
    var query = this.knex(this.table).first();
    query = this._parseFilter(query, _.get(req, 'query.filter'));

    return query
    .count('* as count')
    .then((ret) => _.get(ret, 'count'));
  }

  /**
   * @swagger
   * /db:
   *   get:
   *     tags: ['db']
   *     summary: list available tables
   *     description: list available tables
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: list available tables
   *         schema:
   *           type: array
   *           items:
   *             type: string
   *         examples:
   *           application/json: [ 'test', 'sample' ]
   */
  static list(req, res) { /* jshint unused:false */
    return q(KnexREST.tables);
  }

  _parseSimpleFilter(query, filter) {
    /* csv formatted */
    return _.reduce(_.split(filter, ','), (query, filter) => {
      var i = _.indexOf(filter, ':');
      if (i >= 0) {
        return query.where(this._toDB(filter.substr(0, i)), filter.substr(i + 1));
      }
      throw new HTTPError(400, 'Invalid filter: ' + filter);
    }, query);
  }

  _parseFilter(query, filter) {
    if (_.isString(filter)) {
      query = this._parseSimpleFilter(query, filter);
    }
    else if (_.isArray(filter)) {
      /* express has aggregated multiple arguments together */
      _.forEach(filter, (f) => {
        query = this._parseSimpleFilter(query, f);
      });
    }
    else {
      _.forEach(filter, (value, key) => {
        if (_.isObject(value)) {
          /* has an operator */
          _.forEach(value, (value, operator) => {
            query = query.where(
              this._toDB(key),
              _.get(parseOperators, operator), value);
          });
        }
        else {
          query = query.where(this._toDB(key), value);
        }
      });
    }
    return query;
  }

  _parseSort(query, sort) {
    if (_.isEmpty(sort)) {
      return query;
    }
    return _.reduce(_.split(sort, ','), (query, sort) => {
      var i = _.lastIndexOf(sort, ':');
      if (i >= 0) {
        let
          column = sort.substr(0, i),
          direction = sort.substr(i + 1);

        if (_.includes([ 'desc', 'asc' ], _.toLower(direction))) {
          return query.orderBy(this._toDB(column), direction);
        }
        /* wrong guess prob a column name */
      }
      else if (_.isString(sort)) {
        return query.orderBy(this._toDB(sort));
      }
      return query;
    }, query);
  }

  _toDB(key) {
    var map = _.get(this.options, [ 'revmapping', key ]);
    if (_.isNil(map)) {
      if (_.get(this.options, 'strict') && _.has(this.options, 'revmapping')) {
        throw new HTTPError(400, 'Invalid key: ' + key);
      }
      return key;
    }
    return map;
  }

  _fromDB(column) {
    return _.get(this.options, [ 'mapping', column ], column);
  }

  register(app) {
    var path = '/' + this.path;
    app.get(path, wrap(this.search.bind(this)));

    app.get(path + '/count', wrap(this.count.bind(this)));
    if (this.options.key) {
      app.get(path + '/item/:id', wrap(this.get.bind(this)));

      app.get(path + '/item/:id/related', wrap(this.listRelated.bind(this)));
      _.forEach(this.options.related, (desc, rel) => {
        if (_.has(desc, 'join')) {
          app.get(path + '/item/:id/related/' + rel,
            this.relatedJoin.bind(this, '/' + desc.path, desc.key || 'id', /* table mapped primary key default to 'id' */
            desc.join, app));
          app.get(path + '/item/:id/related/' + rel + '/count',
            this.relatedJoin.bind(this, '/' + desc.path + '/count', desc.key || 'id',
            desc.join, app));
        }
        else if (_.has(desc, 'key')) {
          app.get(path + '/item/:id/related/' + rel,
            this.related.bind(this, '/' + desc.path, desc.key, app));
          app.get(path + '/item/:id/related/' + rel + '/count',
            this.related.bind(this, '/' + desc.path + '/count', desc.key, app));
        }
      });
    }

    return this;
  }

  static register(app) {
    app.get('/', wrap(KnexREST.list));
  }
}

module.exports = KnexREST;
