
const
  _ = require('lodash'),
  knex = require('knex'),
  express = require('express'),
  dbInfo = require('./dbInfo.json'),
  dbInfoExtra = require('./dbInfoSwagger.json'),
  KnexRest = require('../BaseKnexREST'),
  KnexRestSwagger = require('../BaseKnexRESTSwagger'),
  Swagger = require('../swagger');

module.exports = {
  router(config) {
    var router = new express.Router();
    var k = knex(config.db);
    var dbSwagger = new KnexRestSwagger(Swagger.getSpec(), dbInfoExtra);

    _.forEach(dbInfo, function(desc, path) {
      var db = new KnexRest(k, path, desc);
      db.register(router);
      dbSwagger.swag(db);
    });

    KnexRest.register(router);
    return router;
  }
};
