const utils = require('nps-utils');

module.exports = {
  scripts: {
    test: {
      default: {
        script: 'mocha test',
        description: 'Run unit-tests' },
      dev: {
        script: utils.crossEnv('multi="mocha-notifier-reporter=- spec=-" nps "test -R mocha-multi"'),
        description: "Run unit-tests in dev environment (notifier)" },
      watch: {
        script: 'nodemon -w src -w test -x "nps test.dev"',
        description: 'Run unit-tests in dev environment and watch for changes' },
      lint: {
        default: {
          script: './bin/lint',
          description: 'Run the linter' },
        watch: 'nodemon -w src -w test -w www/src -x \'nps test.lint\'',
        builder: {
          script: './bin/lint --builder',
          description: 'Run the linter with an easy to grep output' },
        badge: {
          script: './bin/mkbadge',
          description: 'Create a linting badge "./badge.svg" (CI)' },
      },
      coverage: {
        default: 'istanbul cover ./node_modules/.bin/_mocha -- test',
        watch: 'nodemon -w src -w test -x \'nps test.coverage\''
      },
      www: {
        default: 'cd www; nps "test --browsers jsdom"',
        watch: 'cd www; nps test.watch'
      }
    },
    serve: {
      forever: {
        script: `forever start -a -l ntof_forever.log -o ntof_out.log -e ntof_err.log --pidFile ntof.pid --spinSleepTime 30000 --minUptime 5000 src/index.js`,
        description: 'Start the server (suitable for production)'
      },
      default: 'nodemon -e js,json,pug -w src src/index.js',
      dev: 'nodemon -e js,json,pug,vue -w www/src -x \'nps build.www.dev; nps serve.default\''
    },
    build: {
      default: 'nps build.www',
      dev: 'nps build.www.dev',
      www: {
        serve: 'nodemon -e js,json,pug,vue -w src -w www/src -x \'nps build.www.dev; node src\'',
        default: utils.series('cd www', 'nps build'),
        dev: utils.series('cd www', 'nps build.dev')
      },
      clean: utils.series('cd www', 'nps build.clean')
    },
    install: {
      default: utils.series('npm install', 'cd www', 'npm install'),
      stub: {
        script: utils.series('cp config-stub.js config.js', './bin/gen-stub-db.js'),
        description: 'Install stub configuration'
      }
    }
  }
};
