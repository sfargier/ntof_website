
import _ from 'lodash';
/*
  we have to provide absolute path since gsap doesn't have browserify directives in its package.json for instance,
  see: https://greensock.com/forums/topic/18681-syntaxerror-import-and-export-may-only-appear-at-the-top-level-220-while-parsing-node_modulesgsaptweenlitejs/
*/
import { TweenMax, Back, TimelineMax, Power4, Power2 } from '../node_modules/gsap/TweenMax';
// import anime from 'animejs';

function getDom(elt) {
  if (_.has(elt, 'elm')) {
    /* VNode */
    return elt.elm;
  }
  else if (_.has(elt, '$el')) {
    /* VueComponent */
    return elt.$el;
  }
  return elt;
}

/* get the very last argument as a callback, manages an optional parameters */
function getCallback(args) {
  var done = _.last(args);
  return _.isFunction(done) ? done : _.noop;
}

function getStyle(elt) {
  return window.getComputedStyle(elt, null);
}

/*
 * This is used to chain animations on a component
 */
function attach(elt, tl, options) {
  if (_.get(options, 'cancel', false)) {
    if (elt.anim) {
      elt.anim.kill();
    }
    elt.anim = tl;
  }
  else {
    if (!elt.anim) {
      elt.anim = new TimelineMax();
    }

    elt.anim.add(tl);
    if (!elt.anim.isActive()) {
      elt.anim.play();
    }
  }
}

var BaseAnimation = {
  setOpacity: function(elt, opacity) {
    elt = getDom(elt);

    TweenMax.set(elt, { opacity });
  },
  fadeIn: function(elt, options, done) {
    done = getCallback(arguments);
    elt = getDom(elt);
    const tl = new TimelineMax();

    tl.call(() => {
      tl.fromTo(elt, 0.3,
        { opacity: _.get(elt.style, 'opacity', 0) }, /* use current opacity direct styling */
        { opacity: 1, ease: Power2.easeOut, onComplete: done });
    });
    attach(elt, tl, options);
  },
  fadeOut: function(elt, options, done) {
    done = getCallback(arguments);
    elt = getDom(elt);
    const tl = new TimelineMax();

    tl.call(() => {
      tl.to(elt, 0.3,
        { opacity: 0, ease: Power2.easeOut, onComplete: done });
    });
    attach(elt, tl, options);
  },
  popIn: function(elt, done) {
    done = getCallback(arguments);
    elt = getDom(elt);
    const tl = new TimelineMax();
    tl.call(() => {
      TweenMax.set(elt, { clearProps: 'width,height' });
      var style = getStyle(elt);
      TweenMax.set(elt, { scale: 1, width: style.width, height: style.height });
      tl.from(elt, 0.3, { height: 0, scale: 0.001, ease: Back.easeOut });
      tl.set(elt, { clearProps: 'width,height', onComplete: done });
    });
    attach(elt, tl);
  },
  popOut: function(elt, done) {
    done = getCallback(arguments);
    elt = getDom(elt);
    const tl = new TimelineMax();
    tl.call(() => {
      TweenMax.set(elt, { clearProps: 'width,height' });
      var style = getStyle(elt);
      TweenMax.set(elt, { scale: 1, width: style.width, height: style.height });
      tl.to(elt, 0.3, { height: 0, scale: 0.001, ease: Back.easeIn, onComplete: done });
    });
    attach(elt, tl);
  },
  pageIn: function(el, done) {
    done = getCallback(arguments);
    const tl = new TimelineMax({ onComplete: done });
    var style = getStyle(el);
    tl.set(el, {
      position: 'absolute', width: style.width, height: style.height,
      y: window.innerHeight,
      scale: 0.8
    })
    .to(el, 0.5, { y: 0, ease: Power4.easeOut })
    .to(el, 1, { scale: 1, ease: Power4.easeOut })
    .set(el, { clearProps: 'position,width,height' });
  },
  pageOut: function(el, done) {
    done = getCallback(arguments);
    const tl = new TimelineMax({ onComplete: done });
    var style = getStyle(el);
    tl.to(el, 1, {
      position: 'absolute', width: style.width, height: style.height,
      y: window.innerHeight,
      ease: Power4.easeOut
    })
    .set(el, { clearProps: 'position,width,height' });
  }
};

_.assign(BaseAnimation, {
  defaultIn: BaseAnimation.fadeIn,
  defaultOut: BaseAnimation.fadeOut
});

export default BaseAnimation;
