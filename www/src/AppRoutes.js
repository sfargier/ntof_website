
import Home from './AppHome.vue';
import Materials from './AppMaterials.vue';
import SampleInfo from './AppMaterialsSampleInfo.vue';
import Runs from './AppRuns.vue';
import RunsInfo from './AppRunsInfo.vue';

import Test from './Test.vue';

const routes = [
  { name: 'Home', path: '/', component: Home },
  { name: 'Materials', path: '/materials', component: Materials },
  { name: 'MaterialsSample', path: '/materials/sample/:id', component: SampleInfo, navbar: false },
  { name: 'EAR1', path: '/EAR1', component: Runs, props: { area: 'EAR1' } },
  { name: 'EAR2', path: '/EAR2', component: Runs, props: { area: 'EAR2' } },
  { name: 'RunsInfo', path: '/runs/:id', component: RunsInfo, navbar: false },
  { name: 'LogBook', path: '/logbook', component: Test },
  { name: 'Statistics', path: '/stats', component: Test },
  { name: 'Slowinfo', path: '/slowinfo', component: Test },
  { name: 'Documentation', path: '/doc', component: Test },
  { name: 'Users', path: '/users', component: Test },
  { name: 'Shifts', path: '/shifts', component: Test },
  { name: 'Admin', path: '/admin', component: Test }
];

export default routes;
