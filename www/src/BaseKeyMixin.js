
import _ from 'lodash';

function isInput(element) {
  return element.tagName === 'INPUT' || element.tagName === 'SELECT' ||
         element.tagName === 'TEXTAREA' || element.isContentEditable;
}

const keyMap = {
  Enter: 'enter', Escape: 'esc', Shift: 'shift', ' ': 'space',
  ArrowLeft: 'left', ArrowRight: 'right', ArrowUp: 'up', ArrowDown: 'down'
};

const Mod = {
  ctrl: 0x01,
  alt: 0x02,
  shift: 0x04,
  meta: 0x08
};

/**
 * @brief this is a mixin to listen for key events
 * @see https://craig.is/killing/mice for some hints
 */
export default {
  created() {
    this._keyEventHdlr = this._keyEvent.bind(this);
    document.addEventListener('keyup', this._keyEventHdlr);
  },
  beforeDestroy() {
    document.removeEventListener('keyup', this._keyEventHdlr);
  },
  methods: {
    _keyEvent(event) {
      const code =  _.get(keyMap, event.key, event.key);
      const input = isInput(event.target || event.srcElement);
      var mod = 0;

      if (event.altKey) {
        mod |= Mod.alt;
      }
      if (event.ctrlKey) {
        mod |= Mod.ctrl;
      }
      if (event.metaKey) {
        mod |= Mod.meta;
      }

      /* on inputs only fire events when it has a modifier,
         with an exception for escape */
      if (input && !((mod !== 0) || (code === 'esc'))) {
        return;
      }

      if (event.shiftKey) {
        mod |= Mod.shift;
      }
      var cb = _.get(this, [ '_keyCb', code, mod ]);
      _.attempt(cb, event);
    },

    onKey(key, cb) {
      var mods = _.split(key, '-');
      key = mods.pop();

      mods = _.reduce(mods, (ret, m) => (Mod[m] | ret), 0);
      _.set(this, [ '_keyCb', key, mods ], cb);
    },

    onKeyEmit(key, signal) {
      this.onKey(key, (event) => this.$emit(signal || key, event));
    }
  }
};
