var webpackConfig = require('./webpack.config.js')

module.exports = function (karma) {
  karma.set({
    frameworks: [ 'mocha', 'chai' ],

    files: [
      'test/test_index.js'
    ],

    preprocessors: {
      'test/test_index.js': [ 'webpack', 'sourcemap' ]
    },

    webpack: webpackConfig,
    webpackMiddleware: { stats: { colors: karma.colors } },

    reporters: [ 'mocha', 'coverage' ],
    mochaReporter: {
        showDiff: true
    },
    coverageReporter: {
      dir: './coverage',
      reporters: [
        { type: 'lcov', subdir: '.' },
        { type: 'json', subdir: '.' },
        { type: 'text-summary' }
      ]
    },
    // browserDisconnectTimeout: 30000, /* useful for debugging */
    logLevel: karma.DEBUG,
    client: {
        mocha: {
            // grep: 'displays information'
        }
    },

    // Valid values: Chrome Firefox jsdom
    // can also be set on commandline with --browsers ...
    browsers: []
  })
}
