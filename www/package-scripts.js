const utils = require('nps-utils');

module.exports = {
  scripts: {
    build: {
      default: {
        script: utils.crossEnv('NODE_ENV=production webpack --progress --hide-modules'),
        description: 'Build for production' },
      dev: {
        script: utils.crossEnv('NODE_ENV=dev webpack --progress --hide-modules'),
        description: 'Build for debug/dev' },
      clean: 'rm -rf cache'
    },
    test: {
      default: utils.crossEnv('BABEL_ENV=test karma start --single-run'),
      watch: utils.crossEnv('BABEL_ENV=test karma start --browsers jsdom --reporters coverage,mocha,notify')
    }
  }
};
