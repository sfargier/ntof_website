
import { describe, it, beforeEach, afterEach } from 'mocha'
import { expect } from 'chai'
import _ from 'lodash'
import sinon from 'sinon'
import { SuperAgentMock, mount, waitFor } from './mock'
import VueRouter from 'vue-router'

import Comp from '../src/AppMaterials.vue'
import q from 'q'

describe('AppMaterials.vue', function() {
  var sb;

  beforeEach(function() {
    var i = 0;
    sb = sinon.createSandbox();
    sb.sa = new SuperAgentMock(sb);
    sb.sa.addHandler(() => [
      {
        id: 1, title: "Lead", description: "blah",
        element: "Pb", atomicMass: 208,
        setup: null, oldID: null,
        area: 'EAR1', massMg: 1234, diameterMm: 12
      }
    ]);
  });

  afterEach(function() {
    sb.restore();
  });

  it('displays information', function(done) {
    const wrapper = mount(Comp);

    waitFor(wrapper, () => {
      var content = wrapper.find('#content').findAll('.row');
      return (content.length >= 1) ? content : null;
    })
    .then((content) => {
      expect(content.length).to.be.equal(1);
      var cols = content.at(0).findAll('.row>div');
      expect(cols.at(0).text()).equal('1');
      expect(cols.at(3).text()).equal('Lead');
      done();
    })
    .done();
  });

  it('follows route', function(done) {
    var router = new VueRouter();
    router.push({ query: { title: 'pwet' } });
    const wrapper = mount(Comp, { router });

    waitFor(wrapper, () => {
      var content = wrapper.find('#content').findAll('.row');
      return (content.length >= 1) ? content : null;
    })
    .then(() => {
      expect(sb.sa.get.calledOnce).to.be.equal(true);
      var request = sb.sa.get.getCall(0).returnValue;
      expect(request.query.get()).to.deep.equal({
        filter: { title: 'pwet' }, max: 10 });
    })
    .then(() => {
      /* change route, check that vue gets updated */
      wrapper.vm.$router.push({ query: { title: 'toto' } });
      return waitFor(wrapper, () => {
        return sb.sa.get.calledTwice
      });
    })
    .then(() => {
      var request = sb.sa.get.getCall(1).returnValue;
      expect(request.query.get()).to.deep.equal({
        filter: { title: 'toto' }, max: 10 });
      done();
    })
    .done();
  });

  it('displays detailed information', function(done) {
    const wrapper = mount(Comp);

    waitFor(wrapper, () => {
      var content = wrapper.find('#content').findAll('.row');
      return (content.length >= 1) ? content : null;
    })
    .then((content) => {
      content.at(0).trigger('click');
      expect(wrapper.vm.$route.path).to.be.equal('/materials/sample/1');
      done();
    })
    .done();
  });
});
