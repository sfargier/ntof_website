
import _ from 'lodash'
import sinon from 'sinon'
import sa from 'superagent'
import q from 'q'
import VueTests from '@vue/test-utils'
import VueRouter from 'vue-router'

class SuperAgentRequest {
  constructor(url, mock) {
    this.url = url;
    this.mock = mock;
    sinon.spy(this, 'query');
    this.query.get = () => {
      return _.transform(this.query.args,
        (ret, args) => _.assign(ret, args[0]), {});
    }
  }
  query(args) {
    return this;
  }

  then(success, failure) {
    return q()
    .then(() => this.mock._resolve(this))
    .then((ret) => _.has(ret, 'body') ? ret : { body: ret })
    .then(success, failure);
  }


};

class SuperAgentMock {
  constructor(sb) {
    sb = sb || sinon;
    this.handlers = [];
    this.get = sb.stub(sa, 'get');
    this.get.callsFake((url) => {
      return new SuperAgentRequest(url, this);
    });
  }

  addHandler(hdlr) { this.handlers.push(hdlr); }
  clearHandlers() { this.handlers = []; }

  _resolve(req) {
    var ret;
    _.forEach(this.handlers, (h) => {
      ret = h(req);
      return !_.isNil(ret);
    });
    return ret;
  }
};

function waitFor(vm, cb, timeout) {
  if (_.has(vm, 'vm')) {
    vm = vm.vm; /* probably a wrapper */
  }
  var def = q.defer();
  var timer = _.delay(() => def.reject(new Error('timeout')),
    _.defaultTo(timeout, 1000));
  function next() {
    vm.$nextTick(() => {
      // console.log(def.promise)
      if (def.promise.isFulfilled()) { return; }
      try {
        var ret = cb(vm);
        if (ret) {
          clearTimeout(timer);
          def.resolve(ret);
        }
        else {
          _.delay(next, 200);
        }
      }
      catch (e) {
        clearTimeout(timer);
        def.reject(_.has(e, 'message') ? e : new Error(e));
      }
    });
  }
  _.delay(next, 200);
  return def.promise;
}

function createLocalVue() {
  const localVue = VueTests.createLocalVue()
  localVue.use(VueRouter);
  return localVue;
}

function mount(component, options) {
  return VueTests.mount(component, _.assign(
    { localVue: createLocalVue(), router: new VueRouter() }, options));
}

export {
  SuperAgentMock,
  createLocalVue,
  mount, waitFor
};
