
import { describe, it } from 'mocha'
import { mount } from '@vue/test-utils'
import { expect, config } from 'chai'
import _ from 'lodash'

import Comp from '../src/AppMaterialsSearch.vue'

describe('AppMaterialsSearch.vue', function() {
  it('sends events with proper information', function() {
    const wrapper = mount(Comp);
    wrapper.find('button').trigger('click');
    expect(_.get(wrapper.emitted(), 'search[0][0]')).deep.equal({
      materialType: 'samples' /* only this one has a non-empty default value */
    });

    wrapper.find('#materialType').setValue('samples');
    wrapper.find('#atomicMass').setValue('42');
    wrapper.find('#element').setValue('Ge');
    wrapper.find('#title').setValue('title');
    wrapper.find('#desc').setValue('description');
    wrapper.find('button').trigger('click');
    expect(wrapper.emitted().search).lengthOf(2);
    expect(_.get(wrapper.emitted(), 'search[1][0]')).deep.equal({
      materialType: 'samples',
      atomicMass: '42',
      element: 'Ge',
      title: 'title',
      description: 'description'
    });
  });
});
