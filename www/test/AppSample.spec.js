
import { describe, it, beforeEach, afterEach } from 'mocha'
import { expect } from 'chai'
import _ from 'lodash'
import sinon from 'sinon'
import { SuperAgentMock, mount, waitFor } from './mock'
import VueRouter from 'vue-router'

import Comp from '../src/AppMaterialsSample.vue'
import routes from '../src/AppRoutes.js'

describe('AppMaterials.vue', function() {
  var sb;

  beforeEach(function() {
    var i = 0;
    sb = sinon.createSandbox();
    sb.sa = new SuperAgentMock(sb);
    sb.sa.addHandler(() => ({
        id: 1, title: "Lead", description: "blah",
        element: "Pb", atomicMass: 208,
        setup: null, oldID: null,
        area: 'EAR1', massMg: 1234, diameterMm: 12
      }));
  });

  afterEach(function() {
    sb.restore();
  });

  it('displays information', function(done) {
    const wrapper = mount(Comp);

    waitFor(wrapper, () => {
      var content = wrapper.find('.card li').findAll('.row');
      return (content.length >= 1) ? content : null;
    })
    .then((content) => {
      var cols = content.at(1).findAll('.row>div');
      expect(cols.at(1).text()).equal('Lead');
      done();
    })
    .done();
  });

  it('follows route', function(done) {
    var router = new VueRouter({ routes });
    router.push({ path: '/materials/sample/1' });
    const wrapper = mount(Comp, { router });

    waitFor(wrapper, () => {
      var content = wrapper.find('.card li').findAll('.row');
      return (content.length >= 1) ? content : null;
    })
    .then(() => {
      expect(sb.sa.get.calledOnce).to.be.equal(true);
      var request = sb.sa.get.getCall(0).returnValue;
      expect(request.url).to.equal('/db/samples/item/1');
    })
    .then(() => {
      /* change route, check that vue gets updated */
      wrapper.vm.$router.push({ path: '/materials/sample/2' });
      return waitFor(wrapper, () => {
        return sb.sa.get.calledTwice
      });
    })
    .then(() => {
      var request = sb.sa.get.getCall(1).returnValue;
      expect(request.url).to.equal('/db/samples/item/2');
      done();
    })
    .done();
  });
});
