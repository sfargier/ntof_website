
import setimmediate from 'setimmediate'
// See: https://github.com/badeball/karma-jsdom-launcher/issues/6
// event.source is missing on events when using jsdom, let's not use polyfill
global.setImmediate = global.setTimeout;

/* using karma require API */
const testsContext = require.context(".", true, /\.spec\.js$/)

testsContext.keys().forEach(testsContext)
