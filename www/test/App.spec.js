
import { describe, it } from 'mocha'
import { mount, config } from '@vue/test-utils'
import { expect } from 'chai'
import _ from 'lodash'

import App from '../src/App.vue'
import Vue from 'vue'

describe('App.vue', function() {
  it('displays home by default', function() {
    const wrapper = mount(App);

    expect(wrapper.vm).to.have.nested.property('$router.options.routes');
    var routes = _.get(wrapper, 'vm.$router.options.routes[0]');

    expect(wrapper.find({ name: 'AppHome' }).exists(), 'AppHome not found').to.be.equal(true);
    expect(wrapper.find(routes.component).exists()).to.be.equal(true);
  });

  it('can navigate using NavBar', function() {
    const wrapper = mount(App);

    expect(wrapper.vm).to.have.nested.property('$router.options.routes');
    var routes = _.filter(wrapper.vm.$router.options.routes,
      (r) => r.navbar !== false);

    var pages = wrapper.findAll('nav.navbar .nav-link');
    expect(pages).to.have.lengthOf(11);

    for (var i = 0; i < pages.length; ++i) {
      let route = _.get(routes, i);

      pages.at(i).trigger('click');
      expect(wrapper).to.have.nested.property('vm.$route.name', route.name);
      expect(wrapper.find(route.component).exists()).to.be.equal(true);
    }
  });
});
