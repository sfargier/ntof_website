# nTOF public website

nTOF public website

## Production

A working Node.js (including npm) installation is required to compile this app.

To build the application (for production):
```bash
export NODE_ENV=production
# I'd recommend adding this in your .bashrc
export PATH="./node_modules/.bin:$PATH"

# Install dependencies and tools
npm install && nps install

# Build for production
nps build

# Prepare config.js file (see dedicated paragraph)

# Serve forever
nps serve.forever
```

### Configuration

A `config.js` configuration file is required for production, use the following template:
```js
module.exports = {
  "db": {
    "client": "oracledb",
    "connection": {
      "user": "ntof",
      "password": "xxxxx",
      "connectString": "pdb-s.cern.ch:10121/PDB_CERNDB1.cern.ch"
    }
  }
}
```

## Development

To build the application (for dev/debug):
```bash
unset NODE_ENV
# I'd recommend adding this in your .bashrc
export PATH="./node_modules/.bin:$PATH"

# Install dependencies and tools
npm install && nps install

# Prepare config.js file (see 'Stubbing the server')
nps install.stub

# Build (optional, next step will do it again)
nps build.dev

# Start a dev server recompiling whenever needed
nps serve.dev
```

### Stubbing the server

To install stub configuration run:
```bash
nps install.stub
```

**Note:** To modify the stub data, edit `bin/gen-stub-db.js` file.


### Testing

To run unit-tests (in development mode):
```bash
# Run server unit-tests tests
nps test.watch

# Run front-end unit-tests
nps test.www.watch

# Run the linter
nps test.lint.watch
```

## Debugging

To run the server with additional logging:
```bash
export DEBUG=rest:*,app:*,*

# Now run either unit-tests or server
nps serve
```

### Animations

Some hints to debug animations:
```js
// In your browser's console
// Slow down animation engine
TweenMax.globalTimeScale(.1)
```
