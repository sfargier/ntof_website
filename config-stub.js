const path = require('path');

module.exports = {
  db: {
    client: 'sqlite3',
    useNullAsDefault: true,
    connection: { filename: path.join(__dirname, 'db-stub.sql') }
  }
}
