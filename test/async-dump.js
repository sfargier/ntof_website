'use strict';

// const _ = require('lodash');
// const { before, after } = require('mocha');
// const { createHook } = require('async_hooks');
// const { stackTraceFilter } = require('mocha/lib/utils');
// const allResources = new Map();
//
// // this will pull Mocha internals out of the stacks
// const filterStack = stackTraceFilter();
//
// var hook;
//
// before(function() {
//   hook = createHook({
//     init(asyncId, type, triggerAsyncId) {
//       allResources.set(asyncId, {type, triggerAsyncId, stack: (new Error()).stack});
//     },
//     destroy(asyncId) {
//       allResources.delete(asyncId);
//     }
//   }).enable();
// });
//
// after(function() {
//   setTimeout(function() {
//     hook.disable();
//     if (!_.isEmpty(allResources)) {
//       console.error('STUFF STILL IN THE EVENT LOOP:');
//       allResources.forEach(value => {
//         console.error(`Type: ${value.stack[0]}${value.type}`);
//         console.error(filterStack(value.stack));
//         console.error('\n');
//       });
//     }
//   }, 11000);
// });
