
const
  { beforeEach, afterEach } = require('mocha'),
  debug = require('debug')('tests'),
  tmp = require('tmp'),
  path = require('path'),
  knex = require('knex'),
  express = require('express');


module.exports = {
  defaultEnv() {
    var env = {};

    /* app setup */
    beforeEach(function(done) {
      env.app = express();
      env.app.sock = env.app.listen(1234, 'localhost', () => done());
    });

    afterEach(function() {

    });

    /* db setup */
    beforeEach(function() {
      env.dir = tmp.dirSync({ unsafeCleanup: true });
      debug('tmpDir:', env.dir.name);
      var config = {
        client: 'sqlite3',
        useNullAsDefault: true,
        connection: { filename: path.join(env.dir.name, 'db.sql') }
      };

      env.db = knex(config);
    });

    afterEach(function() {
      env.app.sock.close();
      env.app = null;

      env.dir.removeCallback();
      env.dir = null;
      return env.db.destroy()
      .then(() => { env.db = null; });
    });

    return env;
  }
};
