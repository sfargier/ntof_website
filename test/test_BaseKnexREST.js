
const
  { describe, it, beforeEach } = require('mocha'),
  { expect } = require('chai'),
  q = require('q'),
  _ = require('lodash'),
  sa = require('superagent'),
  Helpers = require('./Helpers');

var KnexREST = require('../src/BaseKnexREST'); /* will start the server */

describe('KnexREST tests', function() {

  var env = Helpers.defaultEnv();

  beforeEach(function() {
    return q()
    .then(() => env.db.schema.createTable('testTable', function(table) {
      table.increments();
      table.text('name');
      table.integer('value');
    }))
    .then(() => env.db('testTable').insert([
      { name: 'toto', value: 42 },
      { name: 'titi', value: 43 },
      { name: 'titi', value: 44 }
    ]));
  });

  it('can list databases', function() {
    var service = new KnexREST(env.db, 'testTable');
    service.register(env.app);
    KnexREST.register(env.app); /* base services */

    return sa.get('localhost:1234')
    .then((ret) => {
      expect(ret).to.deep.include({ status: 200, body: [ 'testTable' ] });
    });
  });

  it('can list all items', function() {
    (new KnexREST(env.db, 'testTable')).register(env.app);
    return sa.get('localhost:1234/testTable')
    .then((ret) => {
      expect(ret).to.deep.include({ status: 200, body: [
        { id: 1, name: 'toto', value: 42 },
        { id: 2, name: 'titi', value: 43 },
        { id: 3, name: 'titi', value: 44 }
      ]});
    });
  });

  it('fails to list all items on unknown table', function() {
    return sa.get('localhost:1234/doesNotExist')
    .then(
      () => { throw 'should fail'; },
      (ret) => expect(ret).include({ status: 404 })
    );
  });

  describe('items filtering', function() {
    _.forEach([
      {
        query: { filter: { name: 'titi' } },
        reply: [
          { id: 2, name: 'titi', value: 43 },
          { id: 3, name: 'titi', value: 44 }
        ]
      },
      {
        query: { filter: { value: { lte: 43 } }, sort: 'name' },
        reply: [
          { id: 2, name: 'titi', value: 43 },
          { id: 1, name: 'toto', value: 42 }
        ]
      },
      {
        query: { filter: { value: { gte: 43 } }, sort: 'value:DESC' },
        reply: [
          { id: 3, name: 'titi', value: 44 },
          { id: 2, name: 'titi', value: 43 }
        ]
      },
      {
        query: { filter: { value: { gte: 43 } }, sort: 'value:ASC' },
        reply: [
          { id: 2, name: 'titi', value: 43 },
          { id: 3, name: 'titi', value: 44 }
        ]
      },
      {
        query: { filter: [ 'name:titi', 'value:43' ] },
        reply: [
          { id: 2, name: 'titi', value: 43 }
        ]
      },
      {
        query: { filter: 'name:titi,value:43' },
        reply: [
          { id: 2, name: 'titi', value: 43 }
        ]
      },
      {
        query: { sort: 'name,value:desc' },
        reply: [
          { id: 3, name: 'titi', value: 44 },
          { id: 2, name: 'titi', value: 43 },
          { id: 1, name: 'toto', value: 42 }
        ]
      },
      {
        query: { sort: [ 'name', 'value:desc' ] },
        reply: [
          { id: 3, name: 'titi', value: 44 },
          { id: 2, name: 'titi', value: 43 },
          { id: 1, name: 'toto', value: 42 }
        ]
      }
    ], function(params) {
      it(`can filter items with: ${JSON.stringify(params.query)}`, function() {
        (new KnexREST(env.db, 'testTable', { key: 'id' })).register(env.app);
        return sa.get('localhost:1234/testTable')
        .query(params.query)
        .then((ret) => {
          expect(ret.body).eql(params.reply);
        });
      });

      it(`can count items with: ${JSON.stringify(params.query)}`, function() {
        (new KnexREST(env.db, 'testTable', { key: 'id' })).register(env.app);
        return sa.get('localhost:1234/testTable/count')
        .query(params.query)
        .then((ret) => {
          expect(ret.body).eql(_.size(params.reply));
        });
      });
    });
  });

  it('can get an item', function() {
    (new KnexREST(env.db, 'testTable', { key: 'id' })).register(env.app);
    return sa.get('localhost:1234/testTable/item/1')
    .then((ret) => {
      expect(ret.status).eql(200);
      expect(ret.body).eql({ id: 1, name: 'toto', value: 42 });
    });
  });

  it('fails to retrieve unknown item', function() {
    (new KnexREST(env.db, 'testTable', { key: 'id' })).register(env.app);
    return sa.get('localhost:1234/testTable/item/1234')
    .then(
      () => { throw 'should fail'; },
      (ret) => expect(ret.status).eql(404)
    );
  });

  it('can count items', function() {
    (new KnexREST(env.db, 'testTable')).register(env.app);
    return sa.get('localhost:1234/testTable/count')
    .then((ret) => {
      expect(ret.body).eql(3);
      expect(ret.status).eql(200);
    });
  });

  it('can map columns', function() {
    (new KnexREST(env.db, 'testTable', {
      key: 'id',
      mapping: { 'name': 'publicName', 'value': 'publicValue' }
    })).register(env.app);
    return sa.get('localhost:1234/testTable')
    .query({ filter: { publicName: 'toto' }, sort: 'publicName' })
    .then((ret) => {
      expect(ret.body).eql([ { id: 1, publicName: 'toto', publicValue: 42 } ]);
      expect(ret.status).eql(200);
    });
  });

  it('strictly maps columns', function() {
    var knexRest = new KnexREST(env.db, 'testTable', {
      key: 'id',
      mapping: { 'name': 'publicName', 'value': 'publicValue' }
    });
    knexRest.register(env.app);
    return q()
    .then(() => {
      /* valid request but not using mapping */
      return sa.get('localhost:1234/testTable')
      .query({ filter: { name: 'toto' }, sort: 'publicName' })
      .then((ret) => {
        expect(ret.body).eql([ { id: 1, publicName: 'toto', publicValue: 42 } ]);
        expect(ret.status).eql(200);
      });
    })
    .then(() => {
      knexRest.options.strict = true; /* turn on strict mode */
      return sa.get('localhost:1234/testTable')
      .query({ filter: { name: 'toto' }, sort: 'publicName' })
      .then(
        () => { throw 'should fail'; },
        (err) => {
          expect(err).have.property('status', 400);
          expect(err.response).have.property('text', 'Invalid key: name');
        }
      );
    });
  });

  it('strictly filters non-mapped columns', function() {
    /* when strict mode is active, filter out unmapped columns */
    var knexRest = new KnexREST(env.db, 'testTable', {
      key: 'id',
      mapping: { 'name': 'publicName' },
      strict: true
    });
    knexRest.register(env.app);
    return q()
    .then(() => {
      return sa.get('localhost:1234/testTable')
      .query({ filter: { publicName: 'toto' } })
      .then((ret) => {
        expect(ret.body).eql([ { id: 1, publicName: 'toto' } ]);
        expect(ret.status).eql(200);
      });
    });
  });

  it('can rename a table', function() {
    (new KnexREST(env.db, 'testTable', { key: 'id', path: 'test' }))
    .register(env.app);
    return sa.get('localhost:1234/test/count')
    .then((ret) => {
      expect(ret.body).eql(3);
      expect(ret.status).eql(200);
    });
  });

  describe('invalid requests', function() {
    _.forEach([
      { query: { sort: 'wrong' }, status: 400, text: 'Invalid key: wrong' },
      { query: { filter: 'bad' }, status: 400, text: 'Invalid filter: bad' },
      { query: { filter: 'bad:42' }, status: 400, text: 'Invalid key: bad' }
    ], function(params) {
      it(`rejects invalid request: ${JSON.stringify(params.query)}`, function() {
        (new KnexREST(env.db, 'testTable', {
          key: 'id', strict: true,
          mapping: { 'name': 'publicName', 'value': 'publicValue' }
        })).register(env.app);
        return sa.get('localhost:1234/testTable')
        .query(params.query)
        .then(
          () => { throw 'should fail'; },
          (err) => {
            expect(err).have.property('status', params.status);
            expect(err.response).have.property('text', params.text);
          }
        );
      });
    });
  });
});
