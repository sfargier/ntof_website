
const
  { describe, it, beforeEach } = require('mocha'),
  { expect } = require('chai'),
  _ = require('lodash'),
  Helpers = require('./Helpers');

var KnexREST = require('../src/BaseKnexREST');
var KnexRESTSwagger = require('../src/BaseKnexRESTSwagger');
var Swagger = require('../src/swagger');

describe('KnexREST docs', function() {

  var env = Helpers.defaultEnv();

  beforeEach(function() {
    env.infoDesc = {
      path: 'infoTable',
      key: 'testID'
    };
  });



  it('can generate doc on empty swagger', function() {
    var srv = new KnexREST(env.db, 'testTable', {
      key: 'id',
      related: {
        info: {
          path: 'infoTable',
          key: 'testID'
        }
      }
    });
    var dbSwagger = new KnexRESTSwagger();
    dbSwagger.swag(srv);
  });

  it('can generate doc on complete swagger', function() {
    var srv = new KnexREST(env.db, 'testTable', {
      key: 'id',
      related: {
        info: {
          path: 'infoTable',
          key: 'testID'
        }
      }
    });
    var dbSwagger = new KnexRESTSwagger(Swagger.getSpec(), {});
    var doc = dbSwagger.swag(srv);
    expect(doc).to.have.nested.property('paths./testTable.get');
  });
});
