
const
  { describe, it, beforeEach } = require('mocha'),
  { expect } = require('chai'),
  q = require('q'),
  sa = require('superagent'),
  Helpers = require('./Helpers');

var KnexREST = require('../src/BaseKnexREST'); /* will start the server */

describe('KnexREST db relations', function() {

  var env = Helpers.defaultEnv();

  beforeEach(function() {
    env.infoDesc = {
      path: 'infoTable',
      key: 'testID'
    };
  });

  beforeEach(function() {
    return q()
    .then(() => env.db.schema.createTable('testTable', function(table) {
      table.increments();
      table.text('name');
      table.integer('value');
    }))
    .then(() => env.db('testTable').insert([
      { id: 1, name: 'toto', value: 42 },
      { id: 2, name: 'titi', value: 43 },
      { id: 3, name: 'titi', value: 44 }
    ]));
  });

  beforeEach(function() {
    return q()
    .then(() => env.db.schema.createTable('infoTable', function(table) {
      table.integer('testID');
      table.text('info');
    }))
    .then(() => env.db('infoTable').insert([
      { testID: 2, info: 'info1' },
      { testID: 2, info: 'info2' }
    ]));
  });

  it('can list relations', function() {
    (new KnexREST(env.db, 'infoTable')).register(env.app);
    (new KnexREST(env.db, 'testTable',
      { key: 'id', related: { info: env.infoDesc } })).register(env.app);

    return sa.get('localhost:1234/testTable/item/2/related')
    .then((ret) => {
      expect(ret).to.deep.include({ status: 200, body: [ 'info' ] });
    });
  });

  it('can list items using a relation', function() {
    (new KnexREST(env.db, 'infoTable')).register(env.app);
    (new KnexREST(env.db, 'testTable',
      { key: 'id', related: { info: env.infoDesc } })).register(env.app);

    return sa.get('localhost:1234/testTable/item/2/related/info')
    .then((ret) => {
      expect(ret).to.deep.include({ status: 200, body: [
        { testID: 2, info: 'info1' },
        { testID: 2, info: 'info2' }
      ]});
    });
  });

  it('can filter items in a relation', function() {
    var rt = new require('express').Router();
    (new KnexREST(env.db, 'infoTable')).register(rt);
    (new KnexREST(env.db, 'testTable',
      { key: 'id', related: { info: env.infoDesc } })).register(rt);
    env.app.use('/pwet', rt);

    return sa.get('localhost:1234/pwet/testTable/item/2/related/info')
    .query({ filter: { info: 'info1' } })
    .then((ret) => {
      expect(ret).to.deep.include({ status: 200, body: [
        { testID: 2, info: 'info1' }
      ]});
    });
  });

  it('can count items in a relation', function() {
      (new KnexREST(env.db, 'infoTable')).register(env.app);
      (new KnexREST(env.db, 'testTable',
        { key: 'id', related: { info: env.infoDesc } })).register(env.app);

      return sa.get('localhost:1234/testTable/item/2/related/info/count')
      .then((ret) => {
        expect(ret).to.deep.include({ status: 200, body: 2 });
      });
  });
});
